CREATE DATABASE question DEFAULT CHARACTER SET utf8;
use question;
create table item_category (
	category_id int primary key auto_increment,
	category_name varchar(256) not null
);
