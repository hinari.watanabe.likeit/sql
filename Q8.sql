select
	i.item_id,
	i.item_name,
	i.item_price,
	c.category_name
from
	item i
inner join
	item_category c
on
	i.category_id = c.category_id;
