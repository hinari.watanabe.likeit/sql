select
	c.category_name,
	sum(i.item_price) as total_price
from
	item i
inner join
	item_category c
on
	i.category_id = c.category_id
group by
	i.category_id
order by
	total_price desc;

